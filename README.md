# GeoGebra au centimètre

Le projet consiste à une page html qui permet de faire quelques réglages sur une figure GeoGebra afin de l'utiliser dans un document.

Je prépare souvent des docs avec GeoGebra, et je suis confronté à la non maitrise des dimensions (pixel, cm..) et au repère qui ne me convienne pas quand il s'agit de l'utiliser dans un document.

Une première astuce est d'utiliser GeoGebra5 plutôt que GeoGebra6. Mais cela ne résout pas tout.

Le projet permet de modifier rapidement :
- Les bornes de la figure
- l'échelle
- le style du repère
- le style de la grille
- le style et la couleur des points
- l'écriture des noms de fonctions (en italique)

Un point important. Le fichier image obtenu a une bonne qualité et comporte l'information de sa résolution.

Le projet nécessite un accès internet, pour utiliser GeoGebra en ligne

Lien directement utilisable :

https://beaubiatdenis-pierre.forge.apps.education.fr/geogebra-au-centimetre/



